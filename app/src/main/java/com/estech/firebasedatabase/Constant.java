package com.estech.firebasedatabase;

/**
 * Qastusoft
 * Created by Sergio on 07/06/2017.
 */

public class Constant {

    public static final String INTENT_TODO = "todointent";


    //SharedPreferences
    public static final String PREF_TODO = "ToDoPrefs" ;
    public static final String PREF_USERNAME = "username" ;
}

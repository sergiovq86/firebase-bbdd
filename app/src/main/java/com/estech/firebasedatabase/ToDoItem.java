package com.estech.firebasedatabase;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Qastusoft
 * Created by Sergio on 05/06/2017.
 */

public class ToDoItem implements Parcelable {

    private String id;
    private String name;
    private String message;
    private String username;
    private boolean completed;

    public ToDoItem(String name, String message, String username, boolean completed) {
        this.name = name;
        this.message = message;
        this.username = username;
        this.completed = completed;
    }

    protected ToDoItem(Parcel in) {
        id = in.readString();
        name = in.readString();
        message = in.readString();
        username = in.readString();
        completed = in.readByte() != 0;
    }

    public static final Creator<ToDoItem> CREATOR = new Creator<ToDoItem>() {
        @Override
        public ToDoItem createFromParcel(Parcel in) {
            return new ToDoItem(in);
        }

        @Override
        public ToDoItem[] newArray(int size) {
            return new ToDoItem[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(message);
        dest.writeString(username);
        dest.writeByte((byte) (completed ? 1 : 0));
    }
}

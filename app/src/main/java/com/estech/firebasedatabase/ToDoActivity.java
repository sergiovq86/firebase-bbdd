package com.estech.firebasedatabase;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

/**
 * Qastusoft
 * Created by Sergio on 05/06/2017.
 */

public class ToDoActivity extends AppCompatActivity {

    private String username;
    private ArrayList<String> phoneNumbers;
    private static final String TAG = "ToDoApp - ToDoActivity";
    private FirebaseFirestore db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.todo_activity);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            username = b.getString(Constant.PREF_USERNAME);
        } else {
            finish();
        }

        db = FirebaseFirestore.getInstance();

        setTitle(getString(R.string.new_item));
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fab_upload = findViewById(R.id.upload);
        fab_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveTodo();
            }
        });
    }

    private void saveTodo() {
        // first section
        // get the data to save in our firebase db
        TextInputLayout editText_title = findViewById(R.id.layout_name);
        EditText edit_title = editText_title.getEditText();
        TextInputLayout editText_desc = findViewById(R.id.layout_desc);
        EditText edit_desc = editText_desc.getEditText();

        if (edit_title != null && edit_desc != null) {

            if (!checkText(edit_title))
                Toast.makeText(this, getString(R.string.new_item_no_title), Toast.LENGTH_SHORT).show();
            else {
                ToDoItem todo = new ToDoItem(edit_title.getText().toString(),
                        edit_desc.getText().toString(), username, false);

                db.collection("TodoList").add(todo);
                finish();
            }
        } else {
            Log.e(TAG, "Algún edittext es null");
            finish();
        }
    }

    private boolean checkText(EditText editText) {
        return !editText.getText().toString().isEmpty();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        try {
            if (item.getItemId() == android.R.id.home) {
                finish();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return super.onOptionsItemSelected(item);
    }
}
